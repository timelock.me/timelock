package main

import (
	"flag"
	"log"

	"timelock.me/timelock/pkg/daemon"
)

func main() {
	var opts daemon.Options
	opts.Default()
	opts.RegisterFlags(flag.CommandLine)
	flag.Parse()
	d := opts.Complete()

	if err := d.Run(); err != nil {
		log.Fatalf("run: %v", err)
	}
}

package main

import (
	"context"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"syscall"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/spf13/cobra"
	"golang.org/x/term"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/protobuf/proto"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/db"
	"timelock.me/timelock/pkg/operations"
)

type OpenVaultOp struct {
	timelockDir string
	name        string

	serverOverride string
}

func (o *OpenVaultOp) Execute() {
	vaultFile := filepath.Join(o.timelockDir, "vaults", o.name)
	vaultBytes, err := ioutil.ReadFile(vaultFile)
	if err != nil {
		log.Fatalf("Unable to read vault %q (%q).", o.name, vaultFile)
	}

	var v db.Vault
	if err := proto.Unmarshal(vaultBytes, &v); err != nil {
		log.Fatalf("Unable to parse vault %q: %v", o.name, err)
	}

	server := ""
	if o.serverOverride != "" {
		server = o.serverOverride
	} else {
		server = v.ServerLocation
	}
	if server == "" {
		log.Fatal("No server recorded in the vault. Use the --server-override flag.")
	}

	fmt.Print("Enter a PIN: ")
	pin, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		log.Fatalf("could not read pin: %v")
	}
	fmt.Print("\nEnter the same PIN again: ")
	pin2, err := term.ReadPassword(int(syscall.Stdin))
	fmt.Println("")
	if !cmp.Equal(pin, pin2) {
		log.Fatalf("pins do not match.")
	}

	vo, err := operations.OpenVault(&v, pin)
	if err != nil {
		log.Fatalf("Could not begin opening vault: %v", err)
	}

	conn := dial(server)
	hc := api.NewHasherClient(conn)

	for {
		fmt.Fprintf(os.Stderr, "\rOpening vault. About %v left.     ", vo.EstimatedRemainingTime())
		done, err := vo.Cycle(hc)
		if err != nil {
			// TODO: retry network errors.
			log.Fatalf("An error opening the vault: %v", err)
		}
		if done {
			break
		}
		// Don't send the next request right away since the server won't
		// let us go faster than 1 per 10 seconds anyway.
		time.Sleep(8 * time.Second)
	}
	fmt.Fprintf(os.Stderr, "\n")

	plaintexts, err := vo.Open()
	if err != nil {
		log.Fatalf("Could not open vault: %v", err)
	}

	fmt.Printf("There are %v payloads. The order is arbitrary.\n", len(plaintexts))
	// The *insertion* order is arbitrary. We don't need to scramble the order here.
	for i, pt := range plaintexts {
		fmt.Printf("plaintext %v:\n%s\n---\n", i, pt)
	}
}

type CreateVaultOp struct {
	filepath       string
	server         string
	unlockDuration time.Duration

	timelockDir string
	name        string
}

func dial(dest string) *grpc.ClientConn {
	certPool, err := x509.SystemCertPool()
	if err != nil {
		log.Fatalf("could not process the credentials: %v", err)
	}
	creds := credentials.NewClientTLSFromCert(certPool, "")

	conn, err := grpc.DialContext(context.TODO(), dest, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatalf("Unable to dial server: %v", err)
	}
	return conn
}

func (o *CreateVaultOp) Execute() {
	vaultsDir := filepath.Join(o.timelockDir, "vaults")
	if err := os.MkdirAll(vaultsDir, 0700); err != nil {
		log.Fatalf("Unable to make directory %q: %v", vaultsDir, err)
	}
	vaultFile := filepath.Join(vaultsDir, o.name)
	if _, err := os.Stat(vaultFile); err == nil {
		log.Fatalf("There is already a vault named %q.", o.name)
	}

	fmt.Print("Enter a PIN: ")
	pin, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		log.Fatalf("could not read pin: %v")
	}
	fmt.Print("\nEnter the same PIN again: ")
	pin2, err := term.ReadPassword(int(syscall.Stdin))
	fmt.Println("")
	if err != nil {
		log.Fatalf("could not read pin: %v")
	}
	if !cmp.Equal(pin, pin2) {
		log.Fatalf("pins do not match.")
	}

	conn := dial(o.server)
	hc := api.NewHasherClient(conn)

	vb, err := operations.CreateVault(o.name, pin, o.unlockDuration, hc)
	if err != nil {
		log.Fatalf("unable to initialize vault: %v", err)
	}

	for {
		fmt.Fprintf(os.Stderr, "\rBuilding vault. About %v left.        ", vb.EstimatedRemainingTime())
		done, err := vb.Cycle(hc)
		if err != nil {
			// TODO: retry network errors.
			log.Fatalf("An error building the vault: %v", err)
		}
		if done {
			break
		}
		// Don't send the next request right away since the server won't
		// let us go faster than 1 per 10 seconds anyway.
		time.Sleep(8 * time.Second)
	}
	fmt.Fprintf(os.Stderr, "\n")

	plaintext, err := ioutil.ReadFile(o.filepath)
	if err != nil {
		log.Fatalf("Error reading plaintext: %v", err)
	}

	v, err := vb.Build(plaintext)
	if err != nil {
		log.Fatalf("Error building vault: %v", err)
	}

	v.ServerLocation = o.server

	out, err := proto.Marshal(v)
	if err != nil {
		log.Fatalf("Error marshalling: %v", err)
	}

	if err := ioutil.WriteFile(vaultFile, out, 0600); err != nil {
		log.Fatalf("Error writing vault to disk: %v", err)
	}

	fmt.Println("Vault written.")
}

func VaultCreateCommand() *cobra.Command {
	o := new(CreateVaultOp)
	var cmd = &cobra.Command{
		Use:   "create <vault-name> --server=timelock.me --file=/path/to/file [--unlock-time=2d]",
		Short: "Create a new vault",
		Long: `Create a new vault of the given name, which encrypts the given file.

It will take --unlock-time amount of time to reconstruct the given
file with the command <TODO>.

You should make multiple vaults for each file you want to encrypt,
pointing at different servers. Monitor the health of your vaults
with the command <TODO>. Once you have made enough redundant vaults,
you may delete the --file.

A vault is intended to compliment your offsite backup, not replace it.

Please do not put valuable key material on your hard drive, or even on
your computer at all. There are many ways your computer could accidentally
retain a file even after it is deleted, requiring varying degrees of
sophistication to recover it. For truly sensitive data, consider first
encrypting it on an air-gapped device, and using a vault to encrypt it
a second time.

The system does not perform any checking on the pin you enter, beyond
making you enter it twice initially. This is deliberate, if you enter
the wrong pin under duress, it will take a long time for this to be
evident. You can use the <TODO> command to add a duress pin and determine
what it causes the <TODO> decrypt command to emit.`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			o.timelockDir = cmd.Flag("timelock-dir").Value.String()
			o.name = args[0]
			o.Execute()
		},
	}
	cmd.Flags().StringVar(&o.filepath, "file", "", "the file to encrypt")
	cmd.Flags().StringVar(&o.server, "server", "", "the timelock server to use")
	cmd.Flags().DurationVar(&o.unlockDuration, "unlock-duration", 2*24*time.Hour, "the approximate amount of time decrypting the file should take")
	return cmd
}

func VaultOpenCommand() *cobra.Command {
	o := new(OpenVaultOp)
	var cmd = &cobra.Command{
		Use:   "open <vault-name> [--server-override=timelock.me]",
		Short: "Open a vault and decrypt its contents",
		Long: `Opening the given vault will take an amount of time depending on
the settings it was created with.

The decrypted payloads will be printed to the terminal. If there is more than one payload,
likely only one will decrypt successfully.`,
		Args: cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			o.timelockDir = cmd.Flag("timelock-dir").Value.String()
			o.name = args[0]
			o.Execute()
		},
	}
	cmd.Flags().StringVar(&o.serverOverride, "server-override", "", "the timelock server to use (only needed if the server has changed locations)")
	return cmd
}

func VaultCommands() *cobra.Command {
	var cmd = &cobra.Command{
		Use: "vault",
	}
	cmd.AddCommand(VaultCreateCommand())
	cmd.AddCommand(&cobra.Command{
		Use:   "list",
		Short: "List the stored vaults",
		Run: func(cmd *cobra.Command, args []string) {
			timelockDir := cmd.Flag("timelock-dir").Value.String()
			files, err := ioutil.ReadDir(filepath.Join(timelockDir, "vaults"))
			if err != nil {
				log.Fatalf("Could not read directory: %v", err)
			}
			fmt.Printf("Listing known vaults:\n")
			total := 0
			for _, f := range files {
				if f.IsDir() {
					continue
				}
				fmt.Printf("  %v\n", f.Name())
				total++
			}
			plural := map[bool]string{true: "s", false: ""}
			fmt.Printf("%v known vault%s.\n", total, plural[total != 1])
		},
	})
	cmd.AddCommand(&cobra.Command{
		Use:   "delete vault-name",
		Short: "Delete the given vault",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			timelockDir := cmd.Flag("timelock-dir").Value.String()
			name := args[0]
			v := filepath.Join(timelockDir, "vaults", name)
			if s, err := os.Stat(v); err != nil || s.IsDir() {
				log.Fatalf("There is no vault named %q.", name)
			}
			fmt.Print("Type the name of the vault to confirm your desire to delete it: ")
			read, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				log.Fatalf("Could not read name: %v")
			}
			if name != string(read) {
				log.Fatalf("name didn't match")
			}
			if err := os.Remove(v); err != nil {
				log.Fatalf("error deleting: %v", err)
			}
		},
	})
	cmd.AddCommand(VaultOpenCommand())
	return cmd
}

func main() {
	var rootCmd = &cobra.Command{
		Use:   "timelock-cli",
		Short: "A tool for managing time-locked vaults.",
		Long:  `TODO`,
	}
	rootCmd.PersistentFlags().String("timelock-dir", filepath.Join(os.Getenv("HOME"), ".timelock"), "directory on disk to store database and config")

	rootCmd.AddCommand(VaultCommands())

	rootCmd.Execute()
}

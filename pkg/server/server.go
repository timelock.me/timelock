package server

import (
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/util/clock"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/common"
	"timelock.me/timelock/pkg/db"
)

type Server struct {
	db db.DB

	clock           clock.Clock
	minRequestDelay time.Duration

	api.UnimplementedHasherServer
}

func NewServer(db db.DB, clock clock.Clock) *Server {
	return &Server{
		db:              db,
		clock:           clock,
		minRequestDelay: 10 * time.Second,
	}
}

type ErrMustWait struct {
	WaitTime time.Duration
}

func (e ErrMustWait) Error() string {
	return fmt.Sprintf("must wait %v before your next call", e.WaitTime)
}

func (s *Server) authRequest(a *api.Auth) (*db.Account, error) {
	id := a.AccountId
	acc, err := s.db.FindAccount(id)
	if err != nil {
		return nil, err
	}

	// Time check
	if d := s.minRequestDelay - s.clock.Since(acc.LastAccess); d > 0 {
		if d > 10*time.Second {
			return nil, ErrMustWait{WaitTime: d}
		}
		s.clock.Sleep(s.minRequestDelay - d)
	}

	acc.LastAccess = s.clock.Now()
	acc.Generation++

	if err := s.db.WriteAccount(acc); err != nil {
		return nil, err
	}

	return acc, nil
}

func (s *Server) CreateAccount(ctx context.Context, r *api.CreateAccountRequest) (*api.CreateAccountResponse, error) {
	b, err := common.NewSecret(32)
	if err != nil {
		return nil, err
	}
	id := r.Auth.AccountId
	acc := &db.Account{
		AccountID:    id,
		ServerSecret: b,
		LastAccess:   s.clock.Now(),
	}
	err = s.db.WriteAccount(acc)
	if err != nil {
		return nil, err
	}
	return &api.CreateAccountResponse{}, nil
}

func (s *Server) HMAC(ctx context.Context, r *api.HashRequest) (*api.HashResponse, error) {
	acc, err := s.authRequest(r.Auth)
	if err != nil {
		return nil, err
	}

	hasher := hmac.New(sha512.New512_256, acc.ServerSecret)

	var resp api.HashResponse
	for _, h := range r.Items {
		hasher.Reset()
		hasher.Write(h)
		resp.Items = append(resp.Items,
			&api.HashResponse_Pair{
				Item: h,
				Hmac: hasher.Sum(nil),
			})
	}
	return &resp, nil

}

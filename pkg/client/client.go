package client

import (
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"fmt"

	"github.com/google/go-cmp/cmp"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/common"
)

type Client struct {
	AccountID []byte

	// Never sent to server!
	ClientSecret []byte
}

func (c *Client) CreateAccount(s api.HasherClient) error {
	_, err := s.CreateAccount(context.TODO(), &api.CreateAccountRequest{
		Auth: &api.Auth{
			AccountId: c.AccountID,
		},
	})
	return err
}

type Record struct {
	// Streams counts how many separate streams there are; each stream
	// begins with a new intermediate key.
	Streams int
	// Cycles counts how many server/client hash operations are performed
	// in each stream.
	Cycles int

	// len(Entries) == Streams+1; the last entry is the encrypted secret.
	Entries [][]byte
}

func (c *Client) Encode(s api.HasherClient, secret []byte, streams, cycles int) (*Record, error) {
	// cycles+1 because the first entry is taken up by the random key.
	state := make([][]byte, (cycles+1)*streams)
	at := func(w, c int) *[]byte { return &state[w*(cycles+1)+c] }

	hasher := hmac.New(sha512.New512_256, c.ClientSecret)

	// Initialize each stream with a random key
	for w := 0; w < streams; w++ {
		t, err := common.NewSecret(32)
		if err != nil {
			return nil, err
		}
		*at(w, 0) = t
	}

	// perfom the requested number of cycles of server hash / client hash for each stream
	for cy := 1; cy <= cycles; cy++ {
		req := &api.HashRequest{
			Auth: &api.Auth{
				AccountId: c.AccountID,
			},
		}
		for w := 0; w < streams; w++ {
			req.Items = append(req.Items, *at(w, cy-1))
		}
		resp, err := s.HMAC(context.TODO(), req)
		if err != nil {
			return nil, err
		}
		if len(resp.Items) != streams {
			return nil, fmt.Errorf("got unexpected number of hashes back")
		}
		for w := 0; w < streams; w++ {
			if !cmp.Equal(resp.Items[w].Item, req.Items[w]) {
				return nil, fmt.Errorf("unexpected item or item ordering")
			}

			// Perform the client hash
			hasher.Reset()
			hasher.Write(resp.Items[w].Hmac)
			v := hasher.Sum(nil)

			*at(w, cy) = v
		}
	}

	r := &Record{
		Streams: streams,
		Cycles:  cycles,
	}
	// record the first random key directly
	r.Entries = append(r.Entries, *at(0, 0))
	// Encrypt the subsequent keys with the last state from the prior stream
	for w := 1; w < streams; w++ {
		dst, err := common.Encrypt(*at(w-1, cycles), *at(w, 0))
		if err != nil {
			return nil, err
		}
		r.Entries = append(r.Entries, dst)
	}
	// Finally, encrypt the secret
	dst, err := common.Encrypt(*at(streams-1, cycles), secret)
	if err != nil {
		return nil, err
	}
	r.Entries = append(r.Entries, dst)

	return r, nil
}

func (c *Client) Decode(s api.HasherClient, r *Record) (secret []byte, err error) {
	hasher := hmac.New(sha512.New512_256, c.ClientSecret)

	// begin with the first key, which is not encrypted
	v := r.Entries[0]
	for w := 1; w <= r.Streams; w++ {
		// Find the final value of the stream by performing the
		// requested number of cycles of server hash / client hash
		for cy := 0; cy < r.Cycles; cy++ {
			resp, err := s.HMAC(context.TODO(), &api.HashRequest{
				Auth: &api.Auth{
					AccountId: c.AccountID,
				},
				Items: [][]byte{v},
			})
			if err != nil {
				return nil, err
			}
			if len(resp.Items) != 1 {
				return nil, fmt.Errorf("got wrong number of items")
			}
			hasher.Reset()
			hasher.Write(resp.Items[0].Hmac)
			v = hasher.Sum(nil)
		}

		// Use the value reached after this to decrypt the next key.
		dst, err := common.Decrypt(v, r.Entries[w])
		if err != nil {
			return nil, err
		}
		v = dst
	}
	// The last key is the requested secret.
	return v, nil
}

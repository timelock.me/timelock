package common

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha512"
	"encoding/binary"
)

func Int64ToBytes(i int64) []byte {
	var b [8]byte
	binary.BigEndian.PutUint64(b[:], uint64(i))
	return b[:]
}

func HMAC(key, message []byte) []byte {
	h := hmac.New(sha512.New512_256, key)
	h.Write(message)
	return h.Sum(nil)
}

func NewSecret(length int) ([]byte, error) {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		return nil, err
	}
	return b, nil
}

func CountLeadingZeroBits(b []byte) int {
	bits := 0
	for _, h := range b {
		if h == 0 {
			bits += 8
			continue
		}
		for bit := 7; bit >= 0; bit-- {
			if h&(1<<uint(bit)) != 0 {
				return bits
			}
			bits++
		}

	}
	return bits
}

// Encrypt does AES-<len(key)> in CTR mode. Output will have an IV prepended.
func Encrypt(key []byte, src []byte) ([]byte, error) {
	cyph, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	iv, err := NewSecret(cyph.BlockSize())
	if err != nil {
		return nil, err
	}
	dst := make([]byte, len(src)+len(iv))
	copy(dst[:len(iv)], iv)

	stream := cipher.NewCTR(cyph, iv)
	stream.XORKeyStream(dst[len(iv):], src)
	return dst, nil
}

// Decrypt does AES-<len(key)> in CTR mode. Input must be prefixed with IV.
func Decrypt(key []byte, src []byte) ([]byte, error) {
	cyph, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	iv := src[:cyph.BlockSize()]
	src = src[len(iv):]
	dst := make([]byte, len(src))

	stream := cipher.NewCTR(cyph, iv)
	stream.XORKeyStream(dst, src)
	return dst, nil
}

package daemon

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"flag"
	"fmt"
	"log"
	"math/big"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/crypto/acme/autocert"
	"google.golang.org/grpc"
	"k8s.io/apimachinery/pkg/util/clock"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/db"
	"timelock.me/timelock/pkg/db/badger"
	"timelock.me/timelock/pkg/server"
)

type Options struct {
	HasherPort int
	Hosts      string
	hosts      []string

	Dir string
}

func (o *Options) Default() {
	o.HasherPort = 9789
	o.Dir = filepath.Join(os.Getenv("HOME"), ".timelock")
}

func (o *Options) RegisterFlags(f *flag.FlagSet) {
	f.IntVar(&o.HasherPort, "hasher-port", o.HasherPort, "port to serve the public hashing service on")
	f.StringVar(&o.Dir, "timelock-dir", o.Dir, "directory on disk to store database and config")
	f.StringVar(&o.Hosts, "hosts", o.Hosts, "comma-separated list of hosts to automatically get SSL certs for")
}

func (o *Options) Complete() *Daemon {
	for _, h := range strings.Split(o.Hosts, ",") {
		if h != "" {
			o.hosts = append(o.hosts, h)
		}
	}
	return &Daemon{
		o: *o,
	}
}

type Daemon struct {
	o Options

	db db.DB
}

func (d *Daemon) Run() error {
	err := os.MkdirAll(d.o.Dir, 0775)
	if err != nil {
		return err
	}
	dbPath := filepath.Join(d.o.Dir, "badger")
	db, err := badger.NewBadgerDB(dbPath)
	if err != nil {
		return fmt.Errorf("couldn't open db %q: %v", dbPath, err)
	}
	d.db = db

	hashAddr := fmt.Sprintf(":%v", d.o.HasherPort)
	s := http.Server{
		Addr: hashAddr,
	}

	if len(d.o.hosts) > 0 {
		certdir := filepath.Join(d.o.Dir, "certs")
		if err := os.MkdirAll(certdir, 0775); err != nil {
			return err
		}
		certManager := autocert.Manager{
			Cache:      autocert.DirCache(certdir),
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(d.o.hosts...),
		}
		s.TLSConfig = certManager.TLSConfig()
	} else {
		s.TLSConfig = selfSigned()
	}

	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	hs := server.NewServer(d.db, clock.RealClock{})

	api.RegisterHasherServer(grpcServer, hs)

	s.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.ProtoMajor == 2 && strings.HasPrefix(
			r.Header.Get("Content-Type"), "application/grpc") {
			grpcServer.ServeHTTP(w, r)
			return
		}

		// For now send people to the repo for explanation
		http.Redirect(w, r, "https://gitlab.com/timelock.me/timelock", 301)
	})

	log.Printf("about to listen for hash requests at %v", s.Addr)
	return s.ListenAndServeTLS("", "")
}

func selfSigned() *tls.Config {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2021),
		Subject: pkix.Name{
			Organization:  []string{"self-signed"},
			Country:       []string{""},
			Province:      []string{""},
			Locality:      []string{""},
			StreetAddress: []string{""},
			PostalCode:    []string{""},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Fatalf("Could not gen self-signed key: %v", err)
	}

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	if err != nil {
		log.Fatalf("Could not gen self-signed ca: %v", err)
	}

	caPEM := new(bytes.Buffer)
	pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	log.Printf("Using self-signed ca; trust this cert to avoid SSL errors:\n%s\n", caPEM)

	caPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	})

	// set up our server certificate
	cert := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"self-signed timelock server"},
			Country:       []string{""},
			Province:      []string{""},
			Locality:      []string{""},
			StreetAddress: []string{""},
			PostalCode:    []string{""},
		},
		IPAddresses:  []net.IP{net.IPv4(127, 0, 0, 1), net.IPv6loopback},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(10, 0, 0),
		SubjectKeyId: []byte{1, 2, 3, 4, 6},
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:     x509.KeyUsageDigitalSignature,
	}

	certPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		log.Fatalf("Could not gen self-signed key: %v", err)
	}

	certBytes, err := x509.CreateCertificate(rand.Reader, cert, ca, &certPrivKey.PublicKey, caPrivKey)
	if err != nil {
		log.Fatalf("Could not gen self-signed cert: %v", err)
	}

	certPEM := new(bytes.Buffer)
	pem.Encode(certPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: certBytes,
	})

	certPrivKeyPEM := new(bytes.Buffer)
	pem.Encode(certPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(certPrivKey),
	})

	serverCert, err := tls.X509KeyPair(certPEM.Bytes(), certPrivKeyPEM.Bytes())
	if err != nil {
		log.Fatalf("Could not gen self-signed cert's key pair: %v", err)
	}

	return &tls.Config{
		Certificates: []tls.Certificate{serverCert},
	}
}

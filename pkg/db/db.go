package db

import (
	"errors"
	"time"

	"google.golang.org/protobuf/proto"
)

var ErrNotFound = errors.New("account not found")
var ErrConflict = errors.New("conflicting write, try again")
var ErrOther = errors.New("some other db error")

type DB interface {
	FindAccount(id []byte) (*Account, error)
	WriteAccount(*Account) error
}

// Account tracks knowledge a server tracks about its clients
type Account struct {
	// Enforced by the db layer: each write must increment this by exactly one
	Generation int64

	AccountID []byte
	// LastNonce    int64
	ServerSecret []byte

	LastAccess time.Time
}

func (a *Account) Serialize() ([]byte, error) {
	tb, err := a.LastAccess.MarshalBinary()
	if err != nil {
		return nil, err
	}

	pb := &AccountPB{
		AccountId:      a.AccountID,
		Generation:     a.Generation,
		ServerSecret:   a.ServerSecret,
		LastAccessTime: tb,
	}
	b, err := proto.Marshal(pb)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (a *Account) Deserialize(b []byte) error {
	var pb AccountPB
	err := proto.Unmarshal(b, &pb)
	if err != nil {
		return err
	}

	a.AccountID = pb.AccountId
	a.Generation = pb.Generation
	a.ServerSecret = pb.ServerSecret
	if err := a.LastAccess.UnmarshalBinary(pb.LastAccessTime); err != nil {
		return err
	}

	return nil
}

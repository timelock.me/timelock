package mem

import (
	"sync"

	"timelock.me/timelock/pkg/db"
)

func NewMemoryDB() *InMemoryDB {
	return &InMemoryDB{
		accounts: map[string]*dbRecord{},
	}
}

type dbRecord struct {
	account db.Account
}

type InMemoryDB struct {
	lock     sync.Mutex
	accounts map[string]*dbRecord
}

var _ = db.DB((*InMemoryDB)(nil))

func (d *InMemoryDB) FindAccount(id []byte) (*db.Account, error) {
	d.lock.Lock()
	defer d.lock.Unlock()
	e, ok := d.accounts[string(id)]
	if !ok {
		return nil, db.ErrNotFound
	}
	a := e.account
	return &a, nil
}

func (d *InMemoryDB) WriteAccount(a *db.Account) error {
	d.lock.Lock()
	defer d.lock.Unlock()
	if e, ok := d.accounts[string(a.AccountID)]; ok {
		if a.Generation != e.account.Generation+1 {
			return db.ErrConflict
		}
		e.account = *a
		return nil
	} else {
		if a.Generation != 0 {
			return db.ErrConflict
		}
	}
	d.accounts[string(a.AccountID)] = &dbRecord{account: *a}
	return nil
}

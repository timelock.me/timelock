package badger

import (
	"log"

	"github.com/dgraph-io/badger"
	"timelock.me/timelock/pkg/db"
)

func NewBadgerDB(path string) (*BadgerDB, error) {
	opts := badger.DefaultOptions(path)
	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	return &BadgerDB{
		db: db,
	}, nil
}

func idToKey(id []byte) []byte {
	return append([]byte("client-account/"), id...)
}

type BadgerDB struct {
	db *badger.DB
}

func (d *BadgerDB) FindAccount(id []byte) (*db.Account, error) {
	key := idToKey(id)
	var acc db.Account

	return &acc, translateError(d.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(key)
		if err != nil {
			return err
		}
		return item.Value(func(v []byte) error {
			return acc.Deserialize(v)
		})
	}))
}

func translateError(err error) error {
	switch {
	case err == nil:
		return nil
	case err == badger.ErrKeyNotFound:
		return db.ErrNotFound
	case err == badger.ErrConflict:
		return db.ErrConflict
	default:
		log.Printf("untranslated error %#v", err)
		return db.ErrOther
	}
}

func (d *BadgerDB) WriteAccount(a *db.Account) error {
	key := idToKey(a.AccountID)
	bv, err := a.Serialize()
	if err != nil {
		panic("how could this fail")
	}

	txn := d.db.NewTransaction(true)
	defer txn.Discard()

	tryWrite := func() error {
		if err := txn.Set(key, bv); err != nil {
			return translateError(err)
		}
		return translateError(txn.Commit())
	}

	var old db.Account
	item, err := txn.Get(key)
	if err == badger.ErrKeyNotFound {
		if a.Generation != 0 {
			return db.ErrConflict
		}
		return tryWrite()
	} else if err != nil {
		return translateError(err)
	}

	if err := item.Value(func(v []byte) error {
		return old.Deserialize(v)
	}); err != nil {
		return translateError(err)
	}

	if a.Generation != old.Generation+1 {
		return db.ErrConflict
	}
	return tryWrite()
}

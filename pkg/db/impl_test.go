package db_test

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"timelock.me/timelock/pkg/db"
	"timelock.me/timelock/pkg/db/badger"
	"timelock.me/timelock/pkg/db/mem"
)

func TestMem(t *testing.T) {
	dt := dbTester{mem.NewMemoryDB()}
	dt.testAll(t)
}

func TestBadger(t *testing.T) {
	dir, err := ioutil.TempDir("", "timelock-test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)
	db, err := badger.NewBadgerDB(dir)
	if err != nil {
		t.Fatalf("couldn't create: %v", err)
	}
	dt := dbTester{db}
	dt.testAll(t)
}

type dbTester struct {
	db db.DB
}

func (d *dbTester) testAll(t *testing.T) {
	t.Run("generation", d.testGeneration)
	t.Run("echo", d.testEcho)
}

func (d *dbTester) testGeneration(t *testing.T) {
	a1 := db.Account{
		Generation: 1,
		AccountID:  []byte("account 1"),
	}

	_, err := d.db.FindAccount(a1.AccountID)
	if err != db.ErrNotFound {
		t.Fatalf("unexpected key present, wanted empty database (%v)", err)
	}

	err = d.db.WriteAccount(&a1)
	if err != db.ErrConflict {
		t.Fatalf("should have gotten a conflict because generation should start at 1 (%v)", err)
	}

	if _, err := d.db.FindAccount(a1.AccountID); err != db.ErrNotFound {
		t.Fatalf("unexpected key present, write should have failed (%v)", err)
	}

	a1.Generation = 0
	err = d.db.WriteAccount(&a1)
	if err != nil {
		t.Fatalf("write should have succeeded, but got: %v", err)
	}

	err = d.db.WriteAccount(&a1)
	if err != db.ErrConflict {
		t.Fatalf("should have gotten a conflict because generation did not advance but got (%v)", err)
	}

	a2, err := d.db.FindAccount(a1.AccountID)
	if err != nil {
		t.Fatalf("unexpected fail, key should be there (%v)", err)
	}

	a1.Generation++ // prove that the database made a copy of this.
	a2.Generation++
	err = d.db.WriteAccount(a2)
	if err != nil {
		t.Fatalf("write should have succeeded, but got: %v", err)
	}
}

func (d *dbTester) testEcho(t *testing.T) {
	a1 := db.Account{
		Generation:   0,
		AccountID:    []byte("account 2"),
		ServerSecret: []byte("not really secret"),
		LastAccess:   time.Now(),
	}

	err := d.db.WriteAccount(&a1)
	if err != nil {
		t.Fatalf("write should have succeeded, but got: %v", err)
	}

	a2, err := d.db.FindAccount(a1.AccountID)
	if err != nil {
		t.Fatalf("unexpected fail, key should be there (%v)", err)
	}

	if !cmp.Equal(&a1, a2) {
		t.Errorf("expected a match. \n\n%#v\n\n%#v\n", a1, *a2)
	}
}

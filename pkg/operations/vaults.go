package operations

import (
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"errors"
	"fmt"
	"time"

	"github.com/google/go-cmp/cmp"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/common"
	"timelock.me/timelock/pkg/db"
)

var ErrNeedMoreCycles = errors.New("this vault requires more cycles before it can be built")

type VaultOpener struct {
	vault     *db.Vault
	accountID []byte
	hashedPin []byte

	c int
	s int
	v []byte
}

func OpenVault(vault *db.Vault, pin []byte) (*VaultOpener, error) {
	vo := &VaultOpener{
		vault:     vault,
		accountID: common.HMAC(vault.Salt, []byte(vault.Name)),
		hashedPin: common.HMAC(vault.Salt, pin),
		v:         vault.Entries[0],
	}
	return vo, nil
}

func (o *VaultOpener) EstimatedRemainingTime() time.Duration {
	total := int(o.vault.Cycles) * int(o.vault.Streams)
	current := o.s*int(o.vault.Cycles) + o.c

	return time.Duration(total-current) * 10 * time.Second
}

func (o *VaultOpener) Cycle(hs api.HasherClient) (done bool, err error) {
	if o.s == int(o.vault.Streams) {
		return true, nil
	}
	hasher := hmac.New(sha512.New512_256, o.hashedPin)
	resp, err := hs.HMAC(context.TODO(), &api.HashRequest{
		Auth: &api.Auth{
			AccountId: o.accountID,
		},
		Items: [][]byte{o.v},
	})
	if len(resp.Items) != 1 {
		return false, fmt.Errorf("got wrong number of items")
	}
	hasher.Reset()
	hasher.Write(resp.Items[0].Hmac)

	old := *o

	o.v = hasher.Sum(nil)

	o.c++
	if o.c < int(o.vault.Cycles) {
		return false, nil
	}

	o.c = 0
	o.s++
	if o.s == int(o.vault.Streams) {
		return true, nil
	}
	dst, err := common.Decrypt(o.v, o.vault.Entries[o.s])
	if err != nil {
		o.c = old.c
		o.s = old.s
		o.v = old.v
		return false, err
	}
	o.v = dst
	return false, nil
}

func (o *VaultOpener) Open() (plaintexts [][]byte, err error) {
	for _, ciphertext := range o.vault.Payloads {
		pt, err := common.Decrypt(o.v, ciphertext)
		if err != nil {
			return nil, err
		}
		plaintexts = append(plaintexts, pt)
	}
	return plaintexts, nil
}

type VaultBuilder struct {
	name      string
	accountID []byte
	salt      []byte
	hashedPin []byte // note, we don't want this to hit disk

	cycles  int
	streams int

	streamKeys         [][]byte // this is also sensitive.
	currentStreamValue [][]byte
	currentCycle       int
}

func (b *VaultBuilder) Build(plaintext []byte) (vault *db.Vault, err error) {
	if b.currentCycle < b.cycles {
		return nil, ErrNeedMoreCycles
	}

	var v db.Vault

	v.Name = b.name
	v.Salt = b.salt
	v.Cycles = int32(b.cycles)
	v.Streams = int32(b.streams)

	v.Entries = append(v.Entries, b.streamKeys[0])
	for w := 1; w < b.streams; w++ {
		e, err := common.Encrypt(b.currentStreamValue[w-1], b.streamKeys[w])
		if err != nil {
			return nil, err
		}
		v.Entries = append(v.Entries, e)
	}

	key := b.currentStreamValue[b.streams-1]
	ciphertext, err := common.Encrypt(key, plaintext)
	if err != nil {
		return nil, err
	}
	v.Payloads = append(v.Payloads, ciphertext)

	return &v, nil
}

func (b *VaultBuilder) Cycle(hs api.HasherClient) (done bool, err error) {
	if b.currentCycle == b.cycles {
		return true, nil
	}
	hasher := hmac.New(sha512.New512_256, b.hashedPin)
	req := &api.HashRequest{
		Auth: &api.Auth{
			AccountId: b.accountID,
		},
		Items: b.currentStreamValue,
	}

	resp, err := hs.HMAC(context.TODO(), req)
	if err != nil {
		return false, err
	}
	if len(resp.Items) != b.streams {
		return false, fmt.Errorf("got unexpected number of hashes back")
	}
	for w := 0; w < b.streams; w++ {
		if !cmp.Equal(resp.Items[w].Item, b.currentStreamValue[w]) {
			return false, fmt.Errorf("unexpected item or item ordering")
		}

		// Perform the client hash
		hasher.Reset()
		hasher.Write(resp.Items[w].Hmac)
		v := hasher.Sum(nil)

		b.currentStreamValue[w] = v
	}

	b.currentCycle++
	return b.currentCycle == b.cycles, nil
}

func (b *VaultBuilder) EstimatedRemainingTime() time.Duration {
	return time.Duration(b.cycles-b.currentCycle-1) * 10 * time.Second
}

func CreateVault(name string, pin []byte, unlockDuration time.Duration, hs api.HasherClient) (*VaultBuilder, error) {
	salt, err := common.NewSecret(32)
	if err != nil {
		return nil, err
	}

	totalCycles := int(unlockDuration / (10 * time.Second))
	cycles, streams := splitCycles(totalCycles)

	streamKeys := make([][]byte, streams)
	vals := make([][]byte, streams)
	for i := range streamKeys {
		streamKeys[i], err = common.NewSecret(32)
		if err != nil {
			return nil, err
		}
		vals[i] = streamKeys[i]
	}

	vb := &VaultBuilder{
		name:      name,
		salt:      salt,
		accountID: common.HMAC(salt, []byte(name)),
		hashedPin: common.HMAC(salt, pin),
		cycles:    cycles,
		streams:   streams,

		streamKeys:         streamKeys,
		currentStreamValue: vals,
		currentCycle:       0,
	}

	if _, err := hs.CreateAccount(context.TODO(), &api.CreateAccountRequest{
		Auth: &api.Auth{
			AccountId: vb.accountID,
		},
	}); err != nil {
		return nil, err
	}
	return vb, nil
}

func splitCycles(total int) (cycles, streams int) {
	// I couldn't think of an obviously correct computation so I just hardcoded
	// some things that sounded OK.
	switch {
	case total <= 360:
		return 10, 36
	case total <= 4*360:
		return 20, 72
	case total < 8*360:
		return 30, 96
	case total < 12*360:
		return 45, 96
	case total < 8640:
		return 45, 192
	case total < 2*8640:
		return 90, 192
	case total < 5*8640:
		return 225, 192
	default:
		return 450, 192
	}
}

package integration

import (
	"context"
	"log"
	"net"
	"reflect"
	"testing"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
	"k8s.io/apimachinery/pkg/util/clock"
	"timelock.me/timelock/api"
	"timelock.me/timelock/pkg/client"
	"timelock.me/timelock/pkg/common"
	"timelock.me/timelock/pkg/db/mem"
	"timelock.me/timelock/pkg/operations"
	"timelock.me/timelock/pkg/server"
)

type TimeAdvancingClient struct {
	Clock *clock.FakeClock
	Impl  api.HasherClient
}

func (t *TimeAdvancingClient) CreateAccount(ctx context.Context, in *api.CreateAccountRequest, opts ...grpc.CallOption) (*api.CreateAccountResponse, error) {
	t.Clock.Step(11 * time.Second)
	return t.Impl.CreateAccount(ctx, in, opts...)
}

// POST /api/hmac
func (t *TimeAdvancingClient) HMAC(ctx context.Context, in *api.HashRequest, opts ...grpc.CallOption) (*api.HashResponse, error) {
	t.Clock.Step(11 * time.Second)
	return t.Impl.HMAC(ctx, in, opts...)
}

func TestCorrectness(t *testing.T) {
	db := mem.NewMemoryDB()
	clk := clock.NewFakeClock(time.Now())
	s := server.NewServer(db, clk)

	l := bufconn.Listen(1024 * 1024)
	gs := grpc.NewServer()
	api.RegisterHasherServer(gs, s)
	done := make(chan struct{})
	go func() {
		if err := gs.Serve(l); err != nil {
			log.Fatalf("server error: %v", err)
		}
		close(done)
	}()
	defer func() { <-done }()
	defer gs.Stop()

	conn, err := grpc.DialContext(context.TODO(), "bufnet",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return l.Dial()
		}), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	gc := api.NewHasherClient(conn)

	id, err := common.NewSecret(32)
	if err != nil {
		t.Fatalf("making account id: %v", err)
	}

	b, err := common.NewSecret(32)
	if err != nil {
		t.Fatalf("making secret: %v", err)
	}

	c := client.Client{
		AccountID:    id,
		ClientSecret: b,
	}

	if err := c.CreateAccount(gc); err != nil {
		t.Fatalf("creating account: %v", err)
	}
	clk.Step(11 * time.Second)

	data, err := common.NewSecret(32)
	if err != nil {
		t.Fatalf("making test data: %v", err)
	}

	r, err := c.Encode(&TimeAdvancingClient{Clock: clk, Impl: gc}, data, 20, 20)
	if err != nil {
		t.Fatalf("encode err: %v", err)
	}

	got, err := c.Decode(gc, r)
	if err != nil {
		t.Fatalf("encode err: %v", err)
	}

	if !reflect.DeepEqual(data, got) {
		t.Fatalf("data did not round-trip correctly! %x vs %x", got, data)
	}
}

func TestCorrectnessOperations(t *testing.T) {
	db := mem.NewMemoryDB()
	clk := clock.NewFakeClock(time.Now())
	s := server.NewServer(db, clk)

	l := bufconn.Listen(1024 * 1024)
	gs := grpc.NewServer()
	api.RegisterHasherServer(gs, s)
	done := make(chan struct{})
	go func() {
		if err := gs.Serve(l); err != nil {
			log.Fatalf("server error: %v", err)
		}
		close(done)
	}()
	defer func() { <-done }()
	defer gs.Stop()

	conn, err := grpc.DialContext(context.TODO(), "bufnet",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return l.Dial()
		}), grpc.WithInsecure())
	if err != nil {
		t.Fatalf("Failed to dial bufnet: %v", err)
	}
	defer conn.Close()
	hc := api.NewHasherClient(conn)

	mysecret, err := common.NewSecret(32)
	if err != nil {
		t.Fatalf("making secret: %v", err)
	}

	name := "test-vault"
	pin := []byte("1234")

	vb, err := operations.CreateVault(name, pin, 5*time.Hour, hc)
	if err != nil {
		t.Fatalf("unable to initialize vault: %v", err)
	}

	for {
		clk.Step(11 * time.Second)
		done, err := vb.Cycle(hc)
		if err != nil {
			t.Fatalf("An error building the vault: %v", err)
		}
		if done {
			break
		}
	}

	vault, err := vb.Build(mysecret)
	if err != nil {
		t.Fatalf("Error building vault: %v", err)
	}

	vo, err := operations.OpenVault(vault, pin)
	if err != nil {
		t.Fatalf("Error opening vault: %v", err)
	}
	for {
		clk.Step(11 * time.Second)
		done, err := vo.Cycle(hc)
		if err != nil {
			t.Fatalf("An error opening the vault: %v", err)
		}
		if done {
			break
		}
	}

	plaintexts, err := vo.Open()
	if err != nil {
		t.Fatalf("An error opening the vault: %v", err)
	}
	if g := len(plaintexts); g != 1 {
		t.Fatalf("got wrong number of plaintexts (%v)", g)
	}

	if got := plaintexts[0]; !reflect.DeepEqual(mysecret, got) {
		t.Fatalf("data did not round-trip correctly! %x vs %x", got, mysecret)
	}
}

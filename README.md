# timelock

Timelock is a system for encrypting data, such that accessing the data requires
a certain amount of time.

Example use case: instead of travelling out-of-state to retrieve your crypto
wallet backup, use timelock to encrypt the pin for your hardware wallet with a
significant delay (3 days or a week). Then, it is convenient (ish) to access
your wallet, but it will still be difficult to access under duress.


To build (this is alpha software so it's not easy yet, you have to build it yourself):

```
$ go build -o timelock-cli ./cmd/timelock-cli/main.go
```

To create a "vault":

```
$ ./timelock-cli vault create $VAULT_NAME --server=dns:///timelock.me:443 --file=$FILE_TO_ENCRYPT --unlock-duration=1h

Enter a PIN: 
Enter the same PIN again: 
Building vault. About 0s left.           
Vault written.

```

To open a vault:

```
$ ./timelock-cli vault open $VAULT_NAME
Enter a PIN: 
Enter the same PIN again: 
Opening vault. About 1h0m0s left.

... (1 hour passes) ...

There are 1 payloads. The order is arbitrary.
plaintext 0:
  (file that was encrypted)
```

Note that while it makes sure there is not a typo in your PIN by having you
enter it twice, there is absolutely no way to tell if it is the right PIN or
not until the end. It's a planned feature to add a duress pin, which causes the
payload to decrypt to something valid but not the sensitive material.

## Best practices

**This is early alpha and should not be relied on.**

* Don't use this for anything real yet, seriously
* For each secret you want to encrypt, make multiple vaults, from multiple servers

## How it works

The system requires an external server. The server and client take turns
hashing a piece of data; after enough hashes, the client can use it as the key
to decrypt whatever is stored in its vault. During the creation of a vault, the
hashes can be done in parallel, but to unlock a vault, the hashes must all be
done in serial. https://www.gwern.net/Self-decrypting-files#chained-hashes is a
good description of this approach. In this system, instead of relying on the
difficulty or number of hashes, we rely on the server limiting the client to 1
call every 10 seconds. This makes the unlock time predictable and means it
doesn't take a great deal of CPU power. 

As you can see, the security model relies on you not having access to the
servers you use. But you do not need to trust the server otherwise. 

## Servers

A server is running at https://timelock.me. I may restart it at any time.

You can also run your own server, the code is in this repo. Please send a PR to
list it below.

### Known Servers

* https://timelock.me
* https://timelock.pw (redirects to timelock.me)

## Future plans

* Add duress PINS / decoy payloads
* Add a server-check command, to tell if the server is still around and remembers its key.
* Client: auto-ping servers for all vaults once a month
* Server: drop records for clients that haven't made a call in a year
* Server: make it really easy to operate a server
* Automate everything: combine server and client operations and make this a p2p service.

## Authors

* timelock@timelock.me - I'll stay anonymous for now, thanks.

PRs are welcome, but my time is limited, please be patient.

## License

This is released under the Apache 2.0 license.

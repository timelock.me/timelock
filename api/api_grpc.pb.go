// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// HasherClient is the client API for Hasher service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HasherClient interface {
	// Create an account with this server.
	CreateAccount(ctx context.Context, in *CreateAccountRequest, opts ...grpc.CallOption) (*CreateAccountResponse, error)
	// Request the server perform some hash operations for you.
	HMAC(ctx context.Context, in *HashRequest, opts ...grpc.CallOption) (*HashResponse, error)
}

type hasherClient struct {
	cc grpc.ClientConnInterface
}

func NewHasherClient(cc grpc.ClientConnInterface) HasherClient {
	return &hasherClient{cc}
}

func (c *hasherClient) CreateAccount(ctx context.Context, in *CreateAccountRequest, opts ...grpc.CallOption) (*CreateAccountResponse, error) {
	out := new(CreateAccountResponse)
	err := c.cc.Invoke(ctx, "/Hasher/CreateAccount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *hasherClient) HMAC(ctx context.Context, in *HashRequest, opts ...grpc.CallOption) (*HashResponse, error) {
	out := new(HashResponse)
	err := c.cc.Invoke(ctx, "/Hasher/HMAC", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HasherServer is the server API for Hasher service.
// All implementations must embed UnimplementedHasherServer
// for forward compatibility
type HasherServer interface {
	// Create an account with this server.
	CreateAccount(context.Context, *CreateAccountRequest) (*CreateAccountResponse, error)
	// Request the server perform some hash operations for you.
	HMAC(context.Context, *HashRequest) (*HashResponse, error)
	mustEmbedUnimplementedHasherServer()
}

// UnimplementedHasherServer must be embedded to have forward compatible implementations.
type UnimplementedHasherServer struct {
}

func (UnimplementedHasherServer) CreateAccount(context.Context, *CreateAccountRequest) (*CreateAccountResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAccount not implemented")
}
func (UnimplementedHasherServer) HMAC(context.Context, *HashRequest) (*HashResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method HMAC not implemented")
}
func (UnimplementedHasherServer) mustEmbedUnimplementedHasherServer() {}

// UnsafeHasherServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HasherServer will
// result in compilation errors.
type UnsafeHasherServer interface {
	mustEmbedUnimplementedHasherServer()
}

func RegisterHasherServer(s grpc.ServiceRegistrar, srv HasherServer) {
	s.RegisterService(&Hasher_ServiceDesc, srv)
}

func _Hasher_CreateAccount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateAccountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HasherServer).CreateAccount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Hasher/CreateAccount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HasherServer).CreateAccount(ctx, req.(*CreateAccountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Hasher_HMAC_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HashRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HasherServer).HMAC(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Hasher/HMAC",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HasherServer).HMAC(ctx, req.(*HashRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Hasher_ServiceDesc is the grpc.ServiceDesc for Hasher service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Hasher_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "Hasher",
	HandlerType: (*HasherServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateAccount",
			Handler:    _Hasher_CreateAccount_Handler,
		},
		{
			MethodName: "HMAC",
			Handler:    _Hasher_HMAC_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/api.proto",
}
